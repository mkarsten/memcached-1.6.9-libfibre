#!/bin/sh
BASEDIR=$(dirname "$0")

if [ "$1" = "skiplib" ]; then
	skiplib=skiplib
	shift
fi

[ -z "$1" ] && {
  echo "usage: $0 [skiplib] <path to libfibre> [<path to liburing>]"
  exit 1
}

cd $1
PTLF=$(pwd)
cd -
shift

if [ "$(uname)" = "Linux" ]; then
	SED=sed
	MAKE=make
	count=$(nproc)
	FLIBS="-lfibre"
	if [ -z "$1" ]; then
		[ -f /usr/include/liburing.h ] && FLIBS="$FLIBS -luring"
	else
		[ -f $1/liburing.so ] && FLIBS="$FLIBS -Wl,-rpath=$1 -luring"
		shift
	fi
else
	SED=gsed
	MAKE=gmake
	count=$(sysctl kern.smp.cpus|cut -c16- || echo 1)
	FLIBS="-lfibre -lexecinfo"
fi

[ "$skiplib" = "skiplib" ] || $MAKE -j $count -C $PTLF lib

LDFLAGS="-L $PTLF/src -Wl,-rpath=$PTLF/src" LIBS="$FLIBS" \
CFLAGS="-I$PTLF/src -g -O2 -fno-omit-frame-pointer -fno-stack-clash-protection -DUSE_LIBFIBRE=1" $BASEDIR/configure $*

$SED -i -e "s|-levent ||" Makefile
